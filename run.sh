#!/bin/sh

docker pull rekgrpth/vipnet && \
docker volume create vipnet && \
docker run \
    --hostname vipnet \
    --name vipnet \
    --volume vipnet:/data \
    rekgrpth/vipnet bash
